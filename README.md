A program, written in Java, that parses specific "thmmy" forum's Topics using  
Jsoup and finds embedded videos from posts. Then searches for video's artists,  
names and tags (assuming they are songs) in Last Fm's database using the site's  
api and logs the results in txt files.