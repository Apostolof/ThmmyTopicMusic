class TopicVideoEntry {
	private final String userName, messageUrl, videoId;

	TopicVideoEntry(String userName, String messageUrl, String videoId){
		this.userName = userName;
		this.messageUrl = messageUrl;
		this.videoId = videoId;
	}

	String getUserName(){
		return userName;
	}

	String getMessageUrl(){
		return messageUrl;
	}

	String getVideoId(){
		return videoId;
	}
}
