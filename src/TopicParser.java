import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

class TopicParser {
	static int parseCurrentPageIndex(Document topic) {
		int parsedPage = 1;

		Elements findCurrentPage = topic.select("td:contains(Pages:)>b");

		for (Element item : findCurrentPage) {
			if (!item.text().contains("...") && !item.text().contains("Pages:")) {
				parsedPage = Integer.parseInt(item.text());
				break;
			}
		}

		return parsedPage;
	}

	static int parseTopicNumberOfPages(Document topic) {
		int returnPages = 1;
		Elements pages = topic.select("td:contains(Pages:)>a.navPages");

		if (pages.size() != 0) {
			for (Element item : pages) {
				if (Integer.parseInt(item.text()) > returnPages)
					returnPages = Integer.parseInt(item.text());
			}
		}

		return returnPages;
	}

	static ArrayList<TopicVideoEntry> parseTopic(Document topic) {
		//Method's variables
		ArrayList<TopicVideoEntry> parsedPostsList = new ArrayList<>();
		Elements postRows;

		topic.select("div.quoteheader").remove();
		topic.select("div.quote").remove();
		postRows = topic.select("form[id=quickModForm]>table>tbody>tr:matches(on)");


		for (Element thisRow : postRows) {
			//Variables for Post constructor
			String p_userName, p_meassage_url;

			//Finds subject
			p_meassage_url = thisRow.select("div[id^=subject_]").first().select("a").attr("href");

			Elements noembedTag = thisRow.select("div").select(".post").first().select("noembed");

			Element userName;
			//Finds username
			userName = thisRow.select("a[title^=View the profile of]").first();
			if (userName == null) { //Deleted profile
				p_userName = thisRow
						.select("td:has(div.smalltext:containsOwn(Guest))[style^=overflow]")
						.first().text();
				p_userName = p_userName.substring(0, p_userName.indexOf(" Guest"));
			} else {
				p_userName = userName.html();
			} 

			for (Element _noembed : noembedTag) {
				String tmpEmbededVideosId =_noembed.text().substring(_noembed.text()
						.indexOf("href=\"https://www.youtube.com/watch?") + 38
						, _noembed.text().indexOf("target") - 2);
				parsedPostsList.add(new TopicVideoEntry(p_userName, p_meassage_url, tmpEmbededVideosId));
			}
		}
		return parsedPostsList;
	}
}
