import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class Main {
	private static final ArrayList<Integer> topics = new ArrayList<>();
	private static final String LAST_FM_API_KEY = "";
	private static OkHttpClient client;

	public static void main(String[] args) {
		//topics.add(10563);
		topics.add(53422);

		client = new OkHttpClient();
		long startTime = System.currentTimeMillis();
		for(int topic: topics){
			doTopic(topic);
		}
		System.out.println(System.currentTimeMillis() - startTime);

	}
	private static void doTopic(int topic){
		Request request = new Request.Builder()
				.url("https://www.thmmy.gr/smf/index.php?topic=" + topic + ".0")
				.build();

		Response response = null;
		try {
			int pageNumber = 1;

			for(int i=0; i<1; ++i){
				request = new Request.Builder()
						.url("https://www.thmmy.gr/smf/index.php?topic=" + topic + "." + i * 15)
						.build();

				response = client.newCall(request).execute();
				Document page = Jsoup.parse(response.body().string());
				ArrayList<TopicVideoEntry> list = TopicParser.parseTopic(page);
				if(i == 0) pageNumber = TopicParser.parseTopicNumberOfPages(page);
				response.body().close();

				if (list.isEmpty()) continue;
				File file = new File("/home/apostolof/thmmyMusic/" + topic + "/Page" + (i + 1) + ".txt");
				file.getParentFile().mkdirs();
				PrintWriter writer = new PrintWriter(file, "UTF-8");
				for(TopicVideoEntry entry: list){
					Request youtubeVideoReq = new Request.Builder()
							.url("https://www.youtube.com/watch?v=" + entry.getVideoId())
							.build();

					Response ytVideoResponse = client.newCall(youtubeVideoReq).execute();
					String title = parseTitle(Jsoup.parse(ytVideoResponse.body().string()));
					ytVideoResponse.body().close();
					System.out.println("title = " + title);
					if(title != null){
						Request lastFmRequest = new Request.Builder()
								.url("http://ws.audioscrobbler.com/2.0/?method=track.search&track="
										+ title + "&limit=1&api_key=" + LAST_FM_API_KEY)
								.build();

						Response lastFmResponse = client.newCall(lastFmRequest).execute();
						Document lastFmResults = Jsoup.parse(lastFmResponse.body().string());
						lastFmResponse.body().close();

						Element artist = lastFmResults.select("artist").first();
						Element song = lastFmResults.select("name").first();
						if(artist != null){
							lastFmRequest = new Request.Builder()
									.url("http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist="
											+ URLEncoder.encode(artist.text(), "UTF-8") + "&limit=1&api_key=" + LAST_FM_API_KEY)
									.build();

							lastFmResponse = client.newCall(lastFmRequest).execute();
							lastFmResults = Jsoup.parse(lastFmResponse.body().string());
							lastFmResponse.body().close();

							Elements genreTags = lastFmResults.select("tags tag name");
							if(genreTags != null){
								String tmpTags = "";
								for(Element genre: genreTags) tmpTags += genre.text() + ",";
								tmpTags = tmpTags.substring(0, tmpTags.lastIndexOf(","));
								
								writer.println(entry.getUserName() + "\t"
										+ entry.getMessageUrl()
										+ "\t" + entry.getVideoId()
										+ "\t" + artist.text()
										+ "\t" + song.text()
										+ "\t" + tmpTags);
							}else{
								writer.println(entry.getUserName() + "\t"
										+ entry.getMessageUrl()
										+ "\t" + entry.getVideoId()
										+ "\t" + artist.text()
										+ "\t" + song.text());
							}
						} else{
							writer.println(entry.getUserName() + "\t"
									+ entry.getMessageUrl()
									+ "\t" + entry.getVideoId()
									+ "\t" + null
									+ "\t" + null);
						}
					}
				}
				writer.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			response.body().close();
		}
	}

	static String parseTitle(Document video){
		Element titleSpan = video.select("span.watch-title").first();
		if (titleSpan == null) return null;
		return titleSpan.text().trim().replaceAll("[^Ά-ώa-zA-Z]+", " ");
	}
}
